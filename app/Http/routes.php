<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


$api = app('Dingo\Api\Routing\Router');

$api->version('v1',function($api){
    $api->get('refresh','App\Http\Controllers\Api\TokenController@refresh');
    $api->group(['middleware' => 'jwt.cookies'],function ($api){
        $api->get('/','App\Http\Controllers\Api\UserController@index');
    });
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('login','FrontController@login');

Route::post('auth', 'AuthController@auth');
Route::get('logout','AuthController@logout');

Route::group(['prefix' => 'api'],function(){
    Route::resource('tasks','TaskController');
});

Route::group(['prefix' => 'home','middleware' => 'auth'],function(){
   Route::get('/', 'HomeController@index');
});
