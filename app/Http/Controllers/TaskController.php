<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Task;
class TaskController extends Controller
{
    public function index(){
        $tasks = Task::all();
        return response()->json($tasks);
    }

    public function store(Request $request){
        $task = new Task;
        $task->name = $request->input('name');
        $task->completed = false;
        $task->save();

        return response()->json($task);
    }

    public function update(Request $request,$tasks){
        $task = Task::find($tasks);
        $task->name = $request->input('name');
        $task->completed = $request->input('completed');
        $task->save();

        return response()->json($task);
    }

    public function destroy($tasks){
        $task = Task::find($tasks);
        $task->delete();
        return response()->json('success');
    }
}
