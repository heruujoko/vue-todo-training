<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Cookie;
class AuthController extends Controller
{
    public function auth(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');

        if(Auth::attempt(['email' => $email, 'password' => $password])){
            $user = User::where('email','=',$email)->firstOrFail();
            $customClaims = [
              'iat' => strtotime(Carbon::now()),
              'exp' => strtotime(Carbon::now()->addMinutes(3)),
              'iss' => url('/')
            ];
            $token = JWTAuth::fromUser($user,$customClaims);
            return redirect('home')
                ->withCookie('token_id',$token,null,null,null,false,false);
        } else {
            return redirect('login');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('login')->withCookie(Cookie::forget('token_id'));
    }
}