<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Cookie;
class TokenController extends Controller
{
    public function refresh(Request $request){
        $token = $request->cookie('token_id');
        $refreshed_token = JWTAuth::refresh($token);
        $resp = [
            'status' => 'success',
            'token' => $refreshed_token
        ];
        return response()->json($resp);
    }
}
