<?php

use Illuminate\Database\Seeder;
use App\Task;
class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::create([
           'name' => 'Learn PHP',
            'completed' => true
        ]);
        Task::create([
            'name' => 'Learn Python',
            'completed' => false
        ]);
        Task::create([
            'name' => 'Learn AngularJS',
            'completed' => false
        ]);
        Task::create([
            'name' => 'Learn VueJS',
            'completed' => true
        ]);
    }
}
