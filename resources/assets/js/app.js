import Vue from 'vue'
import Resource from 'vue-resource'
import VTable from './components/VTable.vue'
import Cookies from 'js-cookie'

Vue.config.devtools = true
Vue.use(Resource)
new Vue({
    el: '#app',
    components: {
        'vtable': VTable
    },
    methods: {
        refreshToken(){
            console.log('refreshing token nah');
            this.$http.get('http://laracast-vue.dev/api/refresh').then((response) => {
                var body = JSON.parse(response.body);
                console.log(body);
                var oldCookie = Cookies.get('token_id');
                console.log('deleting old cookie '+oldCookie);
                Cookies.remove('token_id');
                console.log('setting cookies');
                Cookies.set('token_id',body.token);
                var currentCookie = Cookies.get('token_id');
                console.log('current cookies '+currentCookie);
                console.log('done setting cookies');
            });
            setTimeout(this.refreshToken,60000);
        }
    },
    ready(){
        setTimeout(this.refreshToken(),60000);
    }
})