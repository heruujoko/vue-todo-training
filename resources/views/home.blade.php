<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vue HOME</title>
</head>
<body>
    <h2>You are in Home</h2>
    <div id="app">

        <vtable></vtable>
        <button @click="refreshToken">Refresh</button>
    </div>
    <script src="{{ url('js/app.js') }}"></script>
</body>
</html>