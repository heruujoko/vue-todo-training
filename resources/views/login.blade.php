<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vue Sample App</title>
</head>
<body>
    <h2>Please Login</h2>

    <form method="post" action="{{ url('auth') }}">
        <input name="email" type="email" placeholder="email">
        <input name="password" type="password" placeholder="password">
        <button type="submit">login</button>
    </form>
</body>
</html>