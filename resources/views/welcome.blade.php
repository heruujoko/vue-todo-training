<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            li {
                list-style: none;
                text-align: left;
                margin-bottom: 10px;
            }
            .completed {
                text-decoration: line-through;
            }
            .red {
                color: red;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container" id="app">
            <div class="content">
                <tasks :list="taskList"></tasks>
            </div>
        </div>

        <template id="tmp-list">
            <h2>My List (@{{ remaining.length }})</h2>
            <div>
                <input type="text" v-model="newTaskName">
                <button @click="createTask">Add New</button>
                <button @click="hideCompleted">Hide Completed</button>
            </div>
            <div>
                <ul>
                    <li v-for="task in list">
                        <input type="checkbox" v-model="task.completed" @click="updateTask(task)"> <span :class="{'completed' : task.completed }">@{{ task.name }}</span>
                        <span @click="deleteTask(task)" class="red"> X </span>
                    </li>
                </ul>
            </div>
        </template>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.17/vue-resource.js"></script>
        <script>
            var taskComponent = {
                template: '#tmp-list',
                props: ['list'],
                data: {
                   newTaskName: ''
                },
                computed: {
                    remaining: function(){
                        return this.list.filter(function(task){
                            return !task.completed;
                        });
                    }
                },
                methods: {
                    hideCompleted: function(){
                        this.list = this.remaining;
                    },
                    fetchTasks: function(){
                        this.$http.get('http://laracast-vue.dev/api/tasks',function(response){
                            this.list = response;
                        });
                    },
                    createTask: function(){
                        var resource = this.$resource('http://laracast-vue.dev/api/tasks/');
                        var task = {
                          name : this.newTaskName,
                          completed:false
                        };
                        resource.save(task,function(response){
                            console.log(response);
                            this.newTaskName = '';
                            this.fetchTasks();
                        });
                    },
                    updateTask: function(task){
                        task.completed = !task.completed;
                        var resource = this.$resource('http://laracast-vue.dev/api/tasks/:id');
                        resource.update({ id: task.id },task,function(response){
                            console.log(response);
                        });
                    },
                    deleteTask: function(task){
                        var resource = this.$resource('http://laracast-vue.dev/api/tasks/:id');
                        resource.remove({ id: task.id},function(response){
                            this.list.$remove(task);
                        });
                    }
                }
            };

            var vm = new Vue({
                el: '#app',
                data:{
                    taskList: []
                },
                ready: function(){
                    this.$http.get('http://laracast-vue.dev/api/tasks',function(response){
                        this.taskList = response;
                    });
                },
                components: {
                    tasks: taskComponent
                }
            });
        </script>
    </body>
</html>
